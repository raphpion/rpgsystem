package io.raphpion.rpgsystem.infrastructures.repositories;

import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

import static com.mongodb.client.model.Filters.eq;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import io.raphpion.rpgsystem.RPGSystem;

import io.raphpion.rpgsystem.entities.Character;

import io.raphpion.rpgsystem.infrastructures.EntityFactory;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bukkit.Bukkit;

import java.util.Objects;
import java.util.UUID;

public class CharacterRepository extends RepositoryBase {

    private final MongoCollection<Document> characters;

    public CharacterRepository() {
        MongoDatabase database = client.getDatabase(Objects.requireNonNull(RPGSystem.getPlugin(RPGSystem.class).getConfig().getString("db_name")));
        characters = database.getCollection("characters");
    }

    public Character findOrCreate(String p_uuid) {

        Document document = findDocument(p_uuid);

        if (document == null) {
            Bukkit.getLogger().info("No database entry found for player, creating one.");
            document = createDocument(p_uuid);
            characters.insertOne(document);
        }

        Character character = EntityFactory.getCharacter(document);
        save(character);

        return character;
    }

    private Document createDocument(String p_uuid) { return new Document("uuid", p_uuid); }
    private Document findDocument(String p_uuid) { return characters.find(eq("uuid", p_uuid)).first(); }

    public void save(Character p_character) {
        Bson updates = Updates.combine(
                Updates.set("level", p_character.getLevel()),
                Updates.set("experience", p_character.getExperience())
        );

        try {
            UpdateResult result = characters.updateOne(findDocument(p_character.getUUID()), updates);
        } catch (MongoException me) {
            Bukkit.getLogger().warning("§l§6(!) §r§6Unable to update due to an error:" + me);
        }
    }
}
