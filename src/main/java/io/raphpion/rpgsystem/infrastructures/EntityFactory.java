package io.raphpion.rpgsystem.infrastructures;

import io.raphpion.rpgsystem.entities.Character;
import org.bson.Document;

public class EntityFactory {
    public static Character getCharacter(Document p_document) {
        Character character = new Character(p_document.getString("uuid"));

        if (p_document.containsKey("level")) character.setLevel((Integer) p_document.get("level"));
        if (p_document.containsKey("experience")) character.setExperience((Integer) p_document.get("experience"));

        return character;
    }
}
