package io.raphpion.rpgsystem.services;

import io.raphpion.rpgsystem.entities.Character;
import io.raphpion.rpgsystem.infrastructures.repositories.CharacterRepository;

import java.util.UUID;

public class CharacterService {

    private static final CharacterRepository characterRepository = new CharacterRepository();

    public static Character register(String p_uuid) {
        return characterRepository.findOrCreate(p_uuid);
    }

}
