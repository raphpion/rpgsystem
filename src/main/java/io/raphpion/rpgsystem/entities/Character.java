package io.raphpion.rpgsystem.entities;

import org.bson.Document;
import org.bukkit.Bukkit;

public class Character {

    private final String uuid;
    private int level;
    private int experience;

    public Character(String p_uuid) {
        uuid = p_uuid;
        level = 1;
        experience = 0;
    }

    public String getUUID() {
        return uuid;
    }
    public int getLevel() { return level; }
    public int getExperience() { return experience; }

    public void setLevel(int p_level) {
        if (p_level == level) return;
        if (p_level < 1) {
            Bukkit.getLogger().warning("§l§6(!) §r§6Character.setLevel only works with integers superior to 0");
            return;
        }
        level = p_level;
        //TODO: once level curves are created, set experience to p_level's minimum xp
    }

    public void setExperience(int p_experience) {
        if (p_experience == experience) return;
        if (p_experience < 0) {
            Bukkit.getLogger().warning("§l§6(!) §r§6Character.setExperience only works with unsigned integers");
            return;
        }
        experience = p_experience;
        //TODO: once level curves are created, check what level character is now
    }

    public void addExperience(int p_experience) {
        experience += p_experience;
        if (experience < 0) {
            experience = 0;
        }
        //TODO: once level curves are created, check if character gains a level
    }

}
