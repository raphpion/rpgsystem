package io.raphpion.rpgsystem.controllers.listeners;

import io.raphpion.rpgsystem.services.CharacterService;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        CharacterService.register(event.getPlayer().getUniqueId().toString());
    }

}